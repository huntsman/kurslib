﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsFormsApplication1
{
    public static class Helper
    {
        public static DateTime UnixTimeToDateTime(double UnixTime)
        {
            System.DateTime dt = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dt = dt.AddSeconds(UnixTime).ToLocalTime();
            return dt;
        }

        public static double DateTimeToUnixTime(DateTime DateTime)
        {
            return (TimeZoneInfo.ConvertTimeToUtc(DateTime) - new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc)).TotalSeconds;
        }

        public static bool BankRequest(string Telephone, int Cost)
        {
            if (Cost != 0)
                return false;

            return true;
        }
    }
}
