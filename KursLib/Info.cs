﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Data.SQLite;

namespace WindowsFormsApplication1
{
    public partial class Info : Form
    {
        int BookId;
        Main MainForm;
        public Info(int Bookid, Main mf)
        {
            InitializeComponent();
            MainForm = mf;
            BookId = Bookid;
        }

        void LoadData()
        {
            SQLiteDataReader dr = MainForm.DataBase.ReadData("Select Book.Name, Book.About, Book.Image, Book.Cost From Book Where Id = '" + BookId + "';");
            if (dr.HasRows)
            {
                dr.Read();
                NameBox.Text = dr[0].ToString();
                AboutBox.Text = dr[1].ToString();
                pictureBox1.Image = Image.FromStream(new MemoryStream((byte[])dr[2]));
                RentCost.Text = dr[3].ToString();
            }

        }

        private void Info_Shown(object sender, EventArgs e)
        {
            LoadData();
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void RentButton_Click(object sender, EventArgs e)
        {
            MainForm.RForm = new Rent(MainForm.Userid, BookId, MainForm);
            MainForm.RForm.Show();
            this.Close();
        }

    }
}
