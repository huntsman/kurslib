﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Data.SQLite;

namespace WindowsFormsApplication1
{
    public partial class AddEditBook : Form
    {
        bool isAdd;
        int BookId;
        Main MainForm;
        OpenFileDialog OFD;
        MemoryStream SImage;
        MemoryStream SDocument;
        public List<int> CheckList;

        public AddEditBook(bool isAdd, int BookId, Main mform)
        {
            InitializeComponent();
            this.isAdd = isAdd;
            this.BookId = BookId;
            CheckList = new List<int>();
            MainForm = mform;
            SImage = new MemoryStream();
            SDocument = new MemoryStream();
        }

        void LastRow()
        {
            string str;
            SQLiteDataReader dr = MainForm.DataBase.ReadData("SELECT MAX(Id) FROM Book;");
            if (dr.HasRows)
            {
                dr.Read();
                str = dr[0].ToString();
                if (str.Length > 0)
                {
                    BookId = int.Parse(str) + 1;
                }
                else
                    BookId = 1;
            }
        }

        void LoadData()
        {
            CheckList.Clear();
            NameCheck.Checked = true; AboutCheck.Checked = true; ImageCheck.Checked = true; DocCheck.Checked = true; CostCheck.Checked = true;
            SQLiteDataReader dr = MainForm.DataBase.ReadData("Select Book.Name, Book.About, Book.Image, Book.Data, Book.Cost From Book Where Id = '" + BookId + "';");
            if (dr.HasRows)
            {
                dr.Read();
                BookName.Text = dr[0].ToString();
                BookAbout.Text = dr[1].ToString();
                SImage = new MemoryStream((byte[])dr[2]);
                pictureBox1.Image = Image.FromStream(SImage);
                SDocument = new MemoryStream((byte[])dr[3]);
                CostBox.Text = dr[4].ToString();
            }


            dr = MainForm.DataBase.ReadData(@"SELECT DISTINCT Genre.Id
           FROM Book INNER JOIN (Genre INNER JOIN GenreLink ON Genre.Id = GenreLink.Genreid) ON Book.Id = GenreLink.Bookid 
           WHERE Book.Id = '" + BookId + "';");
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    CheckList.Add(int.Parse(dr[0].ToString()));
                }
            }
            MainForm.InsertCheckList();

        }

        private void ModifyImage_Click(object sender, EventArgs e)
        {
            OFD = new OpenFileDialog();
            OFD.Filter = "Картинки (*.jpg)|*.jpg";
            if (OFD.ShowDialog() == DialogResult.OK)
            {
                SImage = new MemoryStream();
                OFD.OpenFile().CopyTo(SImage);
                pictureBox1.Image = Image.FromStream(SImage);
                ImageCheck.Checked = true;
            }

        }

        private void ModifyDoc_Click(object sender, EventArgs e)
        {
            OFD = new OpenFileDialog();
            OFD.Filter = "Документ (*.rtf)|*.rtf";
            if (OFD.ShowDialog() == DialogResult.OK)
            {
                SDocument = new MemoryStream();
                OFD.OpenFile().CopyTo(SDocument);
                DocCheck.Checked = true;
            }
        }

        private void BookName_TextChanged(object sender, EventArgs e)
        {
            if (BookName.Text.Length >= 3)
                NameCheck.Checked = true;
            else
                NameCheck.Checked = false;
        }

        private void CostBox_TextChanged(object sender, EventArgs e)
        {
            if (BookName.Text.Length >= 1)
                CostCheck.Checked = true;
            else
                CostCheck.Checked = false;
        }

        private void BookAbout_TextChanged(object sender, EventArgs e)
        {
            if (BookAbout.Text.Length > 20)
                AboutCheck.Checked = true;
            else
                AboutCheck.Checked = false;
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            if (NameCheck.Checked == true || AboutCheck.Checked == true || ImageCheck.Checked == true || DocCheck.Checked == true || CostCheck.Checked == true)
            {
                GenreLinkReInsert();
                if (isAdd)
                    InsertBook();
                else
                    UpdateBook();
            }
            else
            {
                MessageBox.Show("Заполните форму полностью!");
                return;
            }
            this.Close();
        }

        private void AddEditBook_Shown(object sender, EventArgs e)
        {
            if (isAdd)
                LastRow();
            else
                LoadData();
        }

        void GenreLinkReInsert()
        {
            MainForm.DataBase.WriteData("DELETE FROM GenreLink Where Bookid = '" + BookId + "';");
            foreach (int id in CheckList)
            {
                MainForm.DataBase.WriteData("INSERT INTO GenreLink VALUES ('" + BookId + "','" + id.ToString() + "');");
            }
            MainForm.DataBase.WriteData("INSERT INTO GenreLink VALUES ('" + BookId + "','-1');"); //Нужно для отображения всех книг
        }

        void InsertBook()
        {
            SQLiteCommand command = new SQLiteCommand(MainForm.DataBase.connection);
            command.CommandText = "INSERT INTO Book (Name, About, Image, Data, Cost) VALUES ('" + BookName.Text.Trim() + "','" + BookAbout.Text.Trim() + "', @Image, @Data,'" + CostBox.Text.Trim() + "');";
            command.CommandType = CommandType.Text;
            var dataParameter = new SQLiteParameter("Image", DbType.Binary) { Value = SImage.ToArray() };
            command.Parameters.Add(dataParameter);
            dataParameter = new SQLiteParameter("Data", DbType.Binary) { Value = SDocument.ToArray() };
            command.Parameters.Add(dataParameter);
            command.ExecuteNonQuery();
        }

        void UpdateBook()
        {
            SQLiteCommand command = new SQLiteCommand(MainForm.DataBase.connection);
            command.CommandText = "UPDATE Book SET Name = '" + BookName.Text.Trim() + "', About = '" + BookAbout.Text.Trim() + "', Image = @Image, Data = @Data, Cost = '" + CostBox.Text.Trim() + "' WHERE Id = '" + BookId.ToString() + "';";
            command.CommandType = CommandType.Text;
            var dataParameter = new SQLiteParameter("Image", DbType.Binary) { Value = SImage.ToArray() };
            command.Parameters.Add(dataParameter);
            dataParameter = new SQLiteParameter("Data", DbType.Binary) { Value = SDocument.ToArray() };
            command.Parameters.Add(dataParameter);
            command.ExecuteNonQuery();
        }




    }
}
