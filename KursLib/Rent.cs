﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SQLite;

namespace WindowsFormsApplication1
{
    public partial class Rent : Form
    {
        int UserId;
        int BookId;
        int RentCost;
        int RentTotalCost;
        string Telephone;
        bool Exits;
        Main MainForm;
        public Rent(int Userid, int Bookid, Main f)
        {
            InitializeComponent();
            UserId = Userid;
            BookId = Bookid;
            RentCost = 0;
            Telephone = "";
            Exits = false;
            MainForm = f;
        }

        private void Accept_Click(object sender, EventArgs e)
        {
            if (Exits)
            {
                MessageBox.Show("Данная книга уже арендована!", "Ошибка");
                this.Close();
                return;
            }

            DialogResult dr = MessageBox.Show("Вы точно уверены в том, что хотите арендовать книгу на " + DayCount.Text.Trim() + " дней, за " + TotalCost.Text + " рублей?", "Уверены?", MessageBoxButtons.OKCancel, MessageBoxIcon.Asterisk);
            if (dr == DialogResult.Cancel)
            {
                this.Close();
            }

            if (int.Parse(DayCount.Text) <= 0)
            {
                MessageBox.Show("Неверное количество дней!", "Ошибка");
                return;
            }
            if (Helper.BankRequest(Telephone, RentTotalCost))
            {
                InsertToBase();
                this.Close();
            }
            else
                MessageBox.Show("В аренде отказано!", "Ошибка");

        }

        void InsertToBase()
        {
            DateTime edt = DateTime.Now;
            edt = edt.AddDays(double.Parse(DayCount.Text));
            string str = Helper.DateTimeToUnixTime(DateTime.Now).ToString().Replace(",", ".");
            string str1 = Helper.DateTimeToUnixTime(edt).ToString().Replace(",", ".");
            MainForm.DataBase.WriteData("INSERT INTO UserCatalog VALUES ('" + UserId.ToString() + "','" + BookId.ToString() + "','" + str + "','" + str1 + "');");
        }

        void RentLoad()
        {
            SQLiteDataReader dr = MainForm.DataBase.ReadData("SELECT Book.Cost FROM Book WHERE Book.Id = '" + BookId + "';");
            if (dr.HasRows)
            {
                dr.Read();
                RentCost = int.Parse(dr[0].ToString());
            }
            dr = MainForm.DataBase.ReadData("SELECT User.TelNumber FROM User WHERE User.Id = '" + UserId + "';");
            if (dr.HasRows)
            {
                dr.Read();
                Telephone = dr[0].ToString();
            }
            dr = MainForm.DataBase.ReadData("SELECT COUNT() FROM UserCatalog WHERE UserCatalog.Userid = '" + UserId + "' AND UserCatalog.Bookid = '" + BookId + "';");
            if (dr.HasRows)
            {
                dr.Read();
                int cr = int.Parse(dr[0].ToString());
                if (cr > 0)
                    Exits = true;
            }
        }

        private void DayCount_TextChanged(object sender, EventArgs e)
        {
            if (DayCount.Text.Length > 0)
            {
                RentTotalCost = int.Parse(DayCount.Text) * RentCost;
                TotalCost.Text = RentTotalCost.ToString();
            }
        }

        private void Rent_Shown(object sender, EventArgs e)
        {
            RentLoad();
        }
    }
}
