﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SQLite;

namespace WindowsFormsApplication1
{
    public partial class Registration : Form
    {
        Auth Auth;
        public Registration(Auth Auth)
        {
            InitializeComponent();
            this.Auth = Auth;
        }

        private void Register_Click(object sender, EventArgs e)
        {
            Error.Text = "";

            if (User.Text.Length == 0 || Password.Text.Length == 0 || FirstName.Text.Length == 0 || LastName.Text.Length == 0 || City.Text.Length == 0 || Street.Text.Length == 0 || House.Text.Length == 0 || TelNumber.Text.Length == 0)
            {
                Error.Text = "Не все поля Заполнены!";
                return;
            }

            if (Password.Text.Length < 5)
            {
                Error.Text = "Пароль слишком  короткий!";
                return;
            }
            if (TelNumber.Text.Length < 12)
            {
                Error.Text = "Некорректный номер!";
                return;
            }

            if (InBase("User", "User", User.Text.Trim()))
            {
                Error.Text = "Такой пользователь уже существует!";
                return;
            }

            SaveToDataBase();

            this.Close();
            Auth.Show();
        }


        void SaveToDataBase()
        {
            int AdressBook = 0;
            int Cityid = 0;
            int Streetid = 0;
            int Homeid = 0;

            int ttt = 0;
            if (!InBase("City", "City", City.Text.Trim()))
                Auth.MainForm.DataBase.WriteData("INSERT INTO City (City) VALUES ('" + City.Text.Trim() + "');");
            if (!InBase("Street", "Street", Street.Text.Trim()))
                ttt = Auth.MainForm.DataBase.WriteData("INSERT INTO Street (Street) VALUES ('" + Street.Text.Trim() + "');");
            if (!InBase("Home", "Home", House.Text.Trim()))
                Auth.MainForm.DataBase.WriteData("INSERT INTO Home (Home) VALUES ('" + House.Text.Trim() + "');");

            Cityid = RowId("City", "City", City.Text.Trim());
            Streetid = RowId("Street", "Street", Street.Text.Trim());
            Homeid = RowId("Home", "Home", House.Text.Trim());

            SQLiteDataReader dr = Auth.MainForm.DataBase.ReadData("Select id From AdressBook where Cityid = '" + Cityid + "' AND Streetid = '" + Streetid + "' AND Homeid = '" + Homeid + "';");
            if (dr.HasRows)
            {
                dr.Read();
                AdressBook = int.Parse(dr[0].ToString());
            }
            else
            {
                Auth.MainForm.DataBase.WriteData("INSERT INTO AdressBook (Cityid, Streetid, Homeid) VALUES ('" + Cityid + "', '" + Streetid + "', '" + Homeid + "');");
                dr = Auth.MainForm.DataBase.ReadData("Select id From AdressBook where Cityid = '" + Cityid + "' AND Streetid = '" + Streetid + "' AND Homeid = '" + Homeid + "';");
                dr.Read();
                AdressBook = int.Parse(dr[0].ToString());
            }

            Auth.MainForm.DataBase.WriteData("INSERT INTO User (User, Password, FirstName, LastName, Adress, TelNumber) VALUES ('" + User.Text.Trim() + "', '" + Password.Text.Trim() + "', '" + FirstName.Text.Trim() + "', '" + LastName.Text.Trim() + "', '" + AdressBook + "', '" + TelNumber.Text.Trim() + "');");

        }




        bool InBase(string tab, string column, string data)
        {
            SQLiteDataReader dr = Auth.MainForm.DataBase.ReadData("Select * From " + tab + " where " + column + " = '" + data + "';");
            if (dr.HasRows)
                return true;

            return false;
        }

        int RowId(string tab, string column, string data)
        {
            string str = ""; 
            SQLiteDataReader dr = Auth.MainForm.DataBase.ReadData("Select Id From " + tab + " where " + column + " = '" + data + "';");
            if (dr.HasRows)
            {
                dr.Read();
                str = dr[0].ToString();

                return int.Parse(dr[0].ToString());
            }

            return -1;
        }


        private void City_MouseClick(object sender, MouseEventArgs e)
        {
            City.Items.Clear();
            SQLiteDataReader dr = Auth.MainForm.DataBase.ReadData("Select * From City;");
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    City.Items.Add(dr[1].ToString());

                }
            }
        }

        private void Street_MouseClick(object sender, MouseEventArgs e)
        {
            Street.Items.Clear();
            SQLiteDataReader dr = Auth.MainForm.DataBase.ReadData("SELECT DISTINCT Street.Street FROM City INNER JOIN (Street INNER JOIN AdressBook ON Street.Id = AdressBook.Streetid) ON City.Id = AdressBook.CityId WHERE City.City='" + City.Text.Trim() + "';");

            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Street.Items.Add(dr[0].ToString());

                }
            }
        }

        private void House_MouseClick(object sender, MouseEventArgs e)
        {
            House.Items.Clear();
            SQLiteDataReader dr = Auth.MainForm.DataBase.ReadData("SELECT DISTINCT Home.Home FROM Street INNER JOIN (Home INNER JOIN AdressBook ON Home.Id = AdressBook.Homeid) ON Street.Id = AdressBook.Streetid WHERE Street.Street ='" + Street.Text.Trim() + "';");

            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    House.Items.Add(dr[0].ToString());

                }
            }

        }

        private void Registration_FormClosed(object sender, FormClosedEventArgs e)
        {
            Auth.Show();
        }



    }
}
