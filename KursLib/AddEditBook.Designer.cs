﻿namespace WindowsFormsApplication1
{
    partial class AddEditBook
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ModifyImage = new System.Windows.Forms.ToolStripMenuItem();
            this.ModifyDoc = new System.Windows.Forms.ToolStripMenuItem();
            this.BookName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.BookAbout = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.SaveButton = new System.Windows.Forms.Button();
            this.ImageCheck = new System.Windows.Forms.CheckBox();
            this.NameCheck = new System.Windows.Forms.CheckBox();
            this.AboutCheck = new System.Windows.Forms.CheckBox();
            this.DocCheck = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.CostBox = new System.Windows.Forms.TextBox();
            this.CostCheck = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.ContextMenuStrip = this.contextMenuStrip1;
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(158, 195);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ModifyImage,
            this.ModifyDoc});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.ShowImageMargin = false;
            this.contextMenuStrip1.ShowItemToolTips = false;
            this.contextMenuStrip1.Size = new System.Drawing.Size(159, 48);
            // 
            // ModifyImage
            // 
            this.ModifyImage.Name = "ModifyImage";
            this.ModifyImage.ShowShortcutKeys = false;
            this.ModifyImage.Size = new System.Drawing.Size(158, 22);
            this.ModifyImage.Text = "Изменить картинку";
            this.ModifyImage.Click += new System.EventHandler(this.ModifyImage_Click);
            // 
            // ModifyDoc
            // 
            this.ModifyDoc.Name = "ModifyDoc";
            this.ModifyDoc.Size = new System.Drawing.Size(158, 22);
            this.ModifyDoc.Text = "Изменить документ";
            this.ModifyDoc.Click += new System.EventHandler(this.ModifyDoc_Click);
            // 
            // BookName
            // 
            this.BookName.Location = new System.Drawing.Point(176, 28);
            this.BookName.Name = "BookName";
            this.BookName.Size = new System.Drawing.Size(100, 20);
            this.BookName.TabIndex = 1;
            this.BookName.TextChanged += new System.EventHandler(this.BookName_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(176, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Название:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 210);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Описание:";
            // 
            // BookAbout
            // 
            this.BookAbout.Location = new System.Drawing.Point(12, 226);
            this.BookAbout.Multiline = true;
            this.BookAbout.Name = "BookAbout";
            this.BookAbout.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.BookAbout.Size = new System.Drawing.Size(264, 98);
            this.BookAbout.TabIndex = 4;
            this.BookAbout.TextChanged += new System.EventHandler(this.BookAbout_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 210);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(0, 13);
            this.label3.TabIndex = 6;
            // 
            // SaveButton
            // 
            this.SaveButton.Location = new System.Drawing.Point(176, 197);
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Size = new System.Drawing.Size(97, 23);
            this.SaveButton.TabIndex = 8;
            this.SaveButton.Text = "Сохранить";
            this.SaveButton.UseVisualStyleBackColor = true;
            this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // ImageCheck
            // 
            this.ImageCheck.AutoSize = true;
            this.ImageCheck.BackColor = System.Drawing.SystemColors.Control;
            this.ImageCheck.Enabled = false;
            this.ImageCheck.Location = new System.Drawing.Point(179, 95);
            this.ImageCheck.Name = "ImageCheck";
            this.ImageCheck.Size = new System.Drawing.Size(74, 17);
            this.ImageCheck.TabIndex = 10;
            this.ImageCheck.Text = "Картинка";
            this.ImageCheck.UseVisualStyleBackColor = false;
            // 
            // NameCheck
            // 
            this.NameCheck.AutoSize = true;
            this.NameCheck.Enabled = false;
            this.NameCheck.Location = new System.Drawing.Point(179, 113);
            this.NameCheck.Name = "NameCheck";
            this.NameCheck.Size = new System.Drawing.Size(76, 17);
            this.NameCheck.TabIndex = 11;
            this.NameCheck.Text = "Название";
            this.NameCheck.UseVisualStyleBackColor = true;
            // 
            // AboutCheck
            // 
            this.AboutCheck.AutoSize = true;
            this.AboutCheck.Enabled = false;
            this.AboutCheck.Location = new System.Drawing.Point(179, 130);
            this.AboutCheck.Name = "AboutCheck";
            this.AboutCheck.Size = new System.Drawing.Size(76, 17);
            this.AboutCheck.TabIndex = 12;
            this.AboutCheck.Text = "Описание";
            this.AboutCheck.UseVisualStyleBackColor = true;
            // 
            // DocCheck
            // 
            this.DocCheck.AutoSize = true;
            this.DocCheck.Enabled = false;
            this.DocCheck.Location = new System.Drawing.Point(179, 147);
            this.DocCheck.Name = "DocCheck";
            this.DocCheck.Size = new System.Drawing.Size(77, 17);
            this.DocCheck.TabIndex = 13;
            this.DocCheck.Text = "Документ";
            this.DocCheck.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(176, 55);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 13);
            this.label4.TabIndex = 15;
            this.label4.Text = "Стоимость:";
            // 
            // CostBox
            // 
            this.CostBox.Location = new System.Drawing.Point(176, 71);
            this.CostBox.Name = "CostBox";
            this.CostBox.Size = new System.Drawing.Size(100, 20);
            this.CostBox.TabIndex = 14;
            this.CostBox.TextChanged += new System.EventHandler(this.CostBox_TextChanged);
            // 
            // CostCheck
            // 
            this.CostCheck.AutoSize = true;
            this.CostCheck.Enabled = false;
            this.CostCheck.Location = new System.Drawing.Point(179, 164);
            this.CostCheck.Name = "CostCheck";
            this.CostCheck.Size = new System.Drawing.Size(81, 17);
            this.CostCheck.TabIndex = 16;
            this.CostCheck.Text = "Стоимость";
            this.CostCheck.UseVisualStyleBackColor = true;
            // 
            // AddEditBook
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 328);
            this.Controls.Add(this.CostCheck);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.CostBox);
            this.Controls.Add(this.DocCheck);
            this.Controls.Add(this.AboutCheck);
            this.Controls.Add(this.NameCheck);
            this.Controls.Add(this.ImageCheck);
            this.Controls.Add(this.SaveButton);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.BookAbout);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.BookName);
            this.Controls.Add(this.pictureBox1);
            this.Name = "AddEditBook";
            this.Text = "Редактор";
            this.Shown += new System.EventHandler(this.AddEditBook_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox BookName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox BookAbout;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button SaveButton;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem ModifyImage;
        private System.Windows.Forms.ToolStripMenuItem ModifyDoc;
        private System.Windows.Forms.CheckBox ImageCheck;
        private System.Windows.Forms.CheckBox NameCheck;
        private System.Windows.Forms.CheckBox AboutCheck;
        private System.Windows.Forms.CheckBox DocCheck;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox CostBox;
        private System.Windows.Forms.CheckBox CostCheck;
    }
}