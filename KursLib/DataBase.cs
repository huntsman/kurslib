﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Data;
using System.Text;
using System.IO;
using System.Data.Common;

namespace WindowsFormsApplication1
{
    public class DataBase
    {
        SQLiteFactory factory;
        public SQLiteConnection connection;

        public DataBase(string Patch)
        {
            factory = (SQLiteFactory)DbProviderFactories.GetFactory("System.Data.SQLite");
            connection = (SQLiteConnection)factory.CreateConnection();
            connection.ConnectionString = "Data Source = " + Patch; 
        }

        public bool OpenConnection()
        {
            try
            {
                connection.Open();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool CloseConnection()
        {
            try
            {
                connection.Close();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public SQLiteDataReader ReadData(string SqlText)
        {
            SQLiteCommand command = new SQLiteCommand(connection);
            command.CommandText = "" + SqlText;
            command.CommandType = CommandType.Text;

            return command.ExecuteReader();
        }

        public int WriteData(string SqlText)
        {
            SQLiteCommand command = new SQLiteCommand(connection);
            command.CommandText = "" + SqlText;
            command.CommandType = CommandType.Text;
            return command.ExecuteNonQuery();
        }

    }
}
