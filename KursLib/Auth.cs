﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SQLite;

namespace WindowsFormsApplication1
{
    public partial class Auth : Form
    {
        public Main MainForm;
        public Registration Reg;
        bool Ok;
        public Auth(Main f)
        {
            InitializeComponent();
            MainForm = f;
            Reg = new Registration(this);
            Ok = false;
        }

        private void Auth_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (Ok == false)
                MainForm.Close();
        }

        private void RegButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            Reg.Show();
        }

        private void AuthButton_Click(object sender, EventArgs e)
        {
            bool admin;
            if(NameBox.Text.Length == 0 || PassBox.Text.Length == 0)
            {
                MessageBox.Show("Не все поля заполнены!", "Ошибка");
                return;
            }

            SQLiteDataReader dr = MainForm.DataBase.ReadData("Select id, Admin From User where User = '" + NameBox.Text.Trim() + "' AND Password = '" + PassBox.Text.Trim() + "';");
            if (dr.HasRows)
            {
                //Заходим
                dr.Read();
                MainForm.Userid = int.Parse(dr[0].ToString());
                admin = (bool)dr[1];
                Ok = true;
                this.Hide();
                MainForm.Show();
                MainForm.Activate();
                MainForm.Select();

                MainForm.LoadLibrary(admin);

            }
            else
            {
                MessageBox.Show("Пара пользователь, пароль не существует!");
                return;
            }

        }



    }
}
