﻿namespace WindowsFormsApplication1
{
    partial class Main
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.listView1 = new System.Windows.Forms.ListView();
            this.ShowRent = new System.Windows.Forms.CheckBox();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.AdminMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.AddBook = new System.Windows.Forms.ToolStripMenuItem();
            this.ModifyBook = new System.Windows.Forms.ToolStripMenuItem();
            this.DeleteBook = new System.Windows.Forms.ToolStripMenuItem();
            this.UserMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.Open = new System.Windows.Forms.ToolStripMenuItem();
            this.Viewb = new System.Windows.Forms.ToolStripMenuItem();
            this.Rent = new System.Windows.Forms.ToolStripMenuItem();
            this.NextPageButton = new System.Windows.Forms.Button();
            this.PageNumber = new System.Windows.Forms.TextBox();
            this.PrePageButton = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.AdminMenuStrip.SuspendLayout();
            this.UserMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // listView1
            // 
            this.listView1.Location = new System.Drawing.Point(126, 15);
            this.listView1.Margin = new System.Windows.Forms.Padding(8);
            this.listView1.MultiSelect = false;
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(517, 340);
            this.listView1.TabIndex = 0;
            this.listView1.TileSize = new System.Drawing.Size(100, 100);
            this.listView1.UseCompatibleStateImageBehavior = false;
            // 
            // ShowRent
            // 
            this.ShowRent.AutoSize = true;
            this.ShowRent.Location = new System.Drawing.Point(8, 368);
            this.ShowRent.Name = "ShowRent";
            this.ShowRent.Size = new System.Drawing.Size(204, 17);
            this.ShowRent.TabIndex = 1;
            this.ShowRent.Text = "Показывать только арендованные";
            this.ShowRent.UseVisualStyleBackColor = true;
            this.ShowRent.CheckedChanged += new System.EventHandler(this.ShowRent_CheckedChanged);
            // 
            // treeView1
            // 
            this.treeView1.Location = new System.Drawing.Point(6, 15);
            this.treeView1.Name = "treeView1";
            this.treeView1.Size = new System.Drawing.Size(121, 340);
            this.treeView1.TabIndex = 3;
            this.treeView1.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.treeView1_AfterCheck);
            this.treeView1.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView1_AfterSelect);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.listView1);
            this.groupBox1.Controls.Add(this.treeView1);
            this.groupBox1.Location = new System.Drawing.Point(2, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(644, 359);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            // 
            // AdminMenuStrip
            // 
            this.AdminMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.AddBook,
            this.ModifyBook,
            this.DeleteBook});
            this.AdminMenuStrip.Name = "AdminMenuStrip";
            this.AdminMenuStrip.Size = new System.Drawing.Size(155, 70);
            // 
            // AddBook
            // 
            this.AddBook.Name = "AddBook";
            this.AddBook.Size = new System.Drawing.Size(154, 22);
            this.AddBook.Text = "Добавит";
            this.AddBook.Click += new System.EventHandler(this.AddBook_Click);
            // 
            // ModifyBook
            // 
            this.ModifyBook.Name = "ModifyBook";
            this.ModifyBook.Size = new System.Drawing.Size(154, 22);
            this.ModifyBook.Text = "Редактировать";
            this.ModifyBook.Click += new System.EventHandler(this.ModifyBook_Click);
            // 
            // DeleteBook
            // 
            this.DeleteBook.Name = "DeleteBook";
            this.DeleteBook.Size = new System.Drawing.Size(154, 22);
            this.DeleteBook.Text = "Удалить";
            // 
            // UserMenuStrip
            // 
            this.UserMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Open,
            this.Viewb,
            this.Rent});
            this.UserMenuStrip.Name = "UserMenuStrip";
            this.UserMenuStrip.ShowImageMargin = false;
            this.UserMenuStrip.ShowItemToolTips = false;
            this.UserMenuStrip.Size = new System.Drawing.Size(128, 92);
            // 
            // Open
            // 
            this.Open.Name = "Open";
            this.Open.Size = new System.Drawing.Size(113, 22);
            this.Open.Text = "Открыть";
            this.Open.Visible = false;
            this.Open.Click += new System.EventHandler(this.Open_Click);
            // 
            // Viewb
            // 
            this.Viewb.Name = "Viewb";
            this.Viewb.Size = new System.Drawing.Size(127, 22);
            this.Viewb.Text = "Просмотреть";
            this.Viewb.Click += new System.EventHandler(this.Viewb_Click);
            // 
            // Rent
            // 
            this.Rent.Name = "Rent";
            this.Rent.Size = new System.Drawing.Size(113, 22);
            this.Rent.Text = "Арендовать";
            this.Rent.Click += new System.EventHandler(this.Rent_Click);
            // 
            // NextPageButton
            // 
            this.NextPageButton.Location = new System.Drawing.Point(415, 365);
            this.NextPageButton.Name = "NextPageButton";
            this.NextPageButton.Size = new System.Drawing.Size(22, 23);
            this.NextPageButton.TabIndex = 4;
            this.NextPageButton.Text = ">";
            this.NextPageButton.UseVisualStyleBackColor = true;
            this.NextPageButton.Click += new System.EventHandler(this.NextPageButton_Click);
            // 
            // PageNumber
            // 
            this.PageNumber.Enabled = false;
            this.PageNumber.Location = new System.Drawing.Point(379, 367);
            this.PageNumber.Name = "PageNumber";
            this.PageNumber.ReadOnly = true;
            this.PageNumber.Size = new System.Drawing.Size(30, 20);
            this.PageNumber.TabIndex = 5;
            // 
            // PrePageButton
            // 
            this.PrePageButton.Location = new System.Drawing.Point(351, 365);
            this.PrePageButton.Name = "PrePageButton";
            this.PrePageButton.Size = new System.Drawing.Size(22, 23);
            this.PrePageButton.TabIndex = 6;
            this.PrePageButton.Text = "<";
            this.PrePageButton.UseVisualStyleBackColor = true;
            this.PrePageButton.Click += new System.EventHandler(this.PrePageButton_Click);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(645, 392);
            this.Controls.Add(this.PrePageButton);
            this.Controls.Add(this.PageNumber);
            this.Controls.Add(this.NextPageButton);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.ShowRent);
            this.Name = "Main";
            this.Text = "Библиотека";
            this.Shown += new System.EventHandler(this.Main_Shown);
            this.groupBox1.ResumeLayout(false);
            this.AdminMenuStrip.ResumeLayout(false);
            this.UserMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.CheckBox ShowRent;
        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ContextMenuStrip AdminMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem AddBook;
        private System.Windows.Forms.ToolStripMenuItem ModifyBook;
        private System.Windows.Forms.ToolStripMenuItem DeleteBook;
        private System.Windows.Forms.ContextMenuStrip UserMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem Open;
        private System.Windows.Forms.ToolStripMenuItem Rent;
        private System.Windows.Forms.Button PrePageButton;
        private System.Windows.Forms.TextBox PageNumber;
        private System.Windows.Forms.Button NextPageButton;
        private System.Windows.Forms.ToolStripMenuItem Viewb;

    }
}