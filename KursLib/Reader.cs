﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Data.SQLite;

namespace WindowsFormsApplication1
{
    public partial class Reader : Form
    {
        int BookId;
        Main MainForm;
        MemoryStream Document;
        public Reader(int bookid, Main f)
        {
            InitializeComponent();
            BookId = bookid;
            MainForm = f;
        }

        void FormLoad()
        {
            SQLiteDataReader dr = MainForm.DataBase.ReadData("Select Book.Data From Book Where Id = '" + BookId + "';");
            if (dr.HasRows)
            {
                dr.Read();
                Document = new MemoryStream((byte[])dr[0]);
            }

            richTextBox1.LoadFile(Document, RichTextBoxStreamType.RichText);
        }

        private void Reader_Shown(object sender, EventArgs e)
        {
            FormLoad();
        }
    }
}
