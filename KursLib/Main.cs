﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Data.SQLite;


namespace WindowsFormsApplication1
{
    public partial class Main : Form
    {
        public DataBase DataBase;
        public int Userid;
        ImageList ImageList;
        const int AmountBookInPage = 10; //Количество книг на странице
        const int TimeGoRent = 12; //Время до напоминания
        int MinInRow;
        AddEditBook AEB;
        Info Inf;
        Reader Readf;
        public Rent RForm;

        public Main()
        {
            InitializeComponent();
            RestorePageNumber();
            Userid = 0;
            ImageList = new System.Windows.Forms.ImageList();
            ImageList.ImageSize = new System.Drawing.Size(72, 128);
            ImageList.ColorDepth = ColorDepth.Depth24Bit;
            DataBase = new DataBase("./1.db");
            Auth Auth = new Auth(this);
            Auth.Show();
            if (!DataBase.OpenConnection())
                MessageBox.Show("Ошибка соединения");

            LoadData();
        }

        void RestorePageNumber()
        {
            MinInRow = 0;
            PageNumber.Text = "0";
        }

        void LoadData()
        {
            listView1.LargeImageList = ImageList;
        }

        void TimeRentIsEnd()
        {
            DateTime td;
            int id;
            string bookname = "";
            SQLiteDataReader dr = DataBase.ReadData("SELECT UserCatalog.Bookid, UserCatalog.EndUT FROM UserCatalog WHERE UserCatalog.Userid = '" + Userid + "';");
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    id = int.Parse(dr[0].ToString());
                    td = Helper.UnixTimeToDateTime(double.Parse(dr[1].ToString()));
                    double hh = td.Subtract(DateTime.Now).TotalHours;
                    if (hh <= TimeGoRent)
                    {
                        if (hh <= 0)
                        {
                            DataBase.WriteData("DELETE FROM UserCatalog Where Bookid = '" + id + "';");
                        }
                        else
                        {
                            SQLiteDataReader drr = DataBase.ReadData("SELECT Book.Name FROM Book WHERE Book.Id = '" + id + "';");
                            if (drr.HasRows) { drr.Read(); bookname = drr[0].ToString(); }
                            DialogResult dres = MessageBox.Show("Продлить аренду '" + bookname + "' на 5 дней?", "Срок аренды заканчивается", MessageBoxButtons.OKCancel, MessageBoxIcon.Asterisk);
                            if (dres == DialogResult.OK)
                            {
                                DataBase.WriteData("UPDATE UserCatalog SET BeginUT = '" + Helper.DateTimeToUnixTime(DateTime.Now).ToString().Replace(",", ".") + "', EndUT = '" + Helper.DateTimeToUnixTime(td.AddDays(5)).ToString().Replace(",", ".") + "' WHERE Userid = '" + Userid.ToString() + "' AND Bookid = '" + id.ToString() + "';");
                            }
                        }
                    }
                }
            }
        }

        public void LoadLibrary(bool admin)
        {
            if (admin)
            {
                listView1.ContextMenuStrip = AdminMenuStrip;
                treeView1.CheckBoxes = true;
                ShowRent.Enabled = false;
            }
            else
                listView1.ContextMenuStrip = UserMenuStrip;

            SQLiteDataReader dr = DataBase.ReadData("Select Id, Name From Genre where Genre.[In] = 0;");
            if (dr.HasRows)
            {
              while (dr.Read())
                {
                  SQLiteDataReader drr = DataBase.ReadData("Select Id, Name From Genre where Genre.[In] = '" + dr[0].ToString() + "';");
                  TreeNode tn = new TreeNode(dr[1].ToString());
                  if (drr.HasRows)
                  {
                      while (drr.Read())
                      {
                          tn.Nodes.Add(drr[0].ToString(), drr[1].ToString());
                      }
                  }

                  tn.Name = dr[0].ToString();
                  treeView1.Nodes.Add(tn);
                }
            }

            TimeRentIsEnd();
        }

        private void Main_Shown(object sender, EventArgs e)
        {
            this.Hide();
        }

        int MaxBookCountInGenre;
        string SelectedNode;
        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (((TreeView)sender).SelectedNode != null)
            {
                SelectedNode = e.Node.Name;
                PageLoad();
            }
        }

        void PageLoad()
        {
            SQLiteDataReader dr;
            MaxBookCountInGenre = 0;
            RestorePageNumber();
            AddBooksToPage();

            if (ShowRent.Checked)
            {
                dr = DataBase.ReadData(@"SELECT Count(Book.Id)
                FROM (Book INNER JOIN GenreLink ON Book.Id = GenreLink.Bookid) INNER JOIN UserCatalog ON Book.Id = UserCatalog.BookId
                WHERE UserCatalog.UserId = " + Userid + " AND GenreLink.Genreid = " + SelectedNode + ";");
            }
            else
            {
                dr = DataBase.ReadData("SELECT Count(Bookid) FROM GenreLink WHERE Genreid = " + SelectedNode + ";");

            }
                if (dr.HasRows)
                {
                    dr.Read();
                    MaxBookCountInGenre = int.Parse(dr[0].ToString());
                }
        }

        void AddBooksToPage()
        {
            listView1.Items.Clear();
            ImageList.Images.Clear();
            SQLiteDataReader dr;
            if (ShowRent.Checked)
                dr = AddBookUser();
            else
                dr = AddBookList();

            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    ImageList.Images.Add(Image.FromStream(new MemoryStream((byte[])dr[2])));
                    listView1.Items.Add(dr[0].ToString(), dr[1].ToString(), ImageList.Images.Count - 1);

                }
            }
        }

        SQLiteDataReader AddBookUser()
        {
            SQLiteDataReader dr = DataBase.ReadData(@"SELECT DISTINCT Book.Id, Book.[Name], Book.Image
            FROM Genre INNER JOIN ((Book INNER JOIN UserCatalog ON Book.Id = UserCatalog.BookId) INNER JOIN GenreLink ON Book.Id = GenreLink.Bookid) ON Genre.Id = GenreLink.Genreid
            WHERE Genre.Id = " + SelectedNode + " AND UserCatalog.UserId = " + Userid + " ORDER BY rowid DESC LIMIT " + MinInRow.ToString() + "," + AmountBookInPage.ToString() + ";");
            return dr;
        }

        SQLiteDataReader AddBookList()
        {
            SQLiteDataReader dr = DataBase.ReadData(@"SELECT DISTINCT Book.Id, Book.[Name], Book.Image
                FROM Genre INNER JOIN (Book INNER JOIN GenreLink ON Book.Id = GenreLink.Bookid) ON RowId = GenreLink.Genreid 
                WHERE Genre.Id = " + SelectedNode + " ORDER BY rowid DESC LIMIT " + MinInRow.ToString() + "," + AmountBookInPage.ToString() + ";");
            return dr;
        }

        private void ModifyBook_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count != 0)
            {
                AEB = new AddEditBook(false, int.Parse(listView1.SelectedItems[0].Name), this);
                AEB.Show();
            }
            ReloadCheckList(treeView1.Nodes);
        }

        private void AddBook_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count == 0)
            {
                AEB = new AddEditBook(true, 0, this);
                AEB.Show();
            }
            ReloadCheckList(treeView1.Nodes);
        }

        void ReloadCheckList(TreeNodeCollection nodes)
        {
            if (AEB != null)
            {
                foreach (TreeNode tn in nodes)
                {
                    if (tn.Checked)
                        AEB.CheckList.Add(int.Parse(tn.Name));
                    if (tn.Nodes.Count > 0)
                        ReloadCheckList(tn.Nodes);

                }
            }
        }

        public void InsertCheckList()
        {
            t_InsertCheckList(treeView1.Nodes);
        }

        void t_InsertCheckList(TreeNodeCollection nodes)
        {
            foreach (TreeNode tn in nodes)
            {
                    if (AEB.CheckList.Find(x => x == int.Parse(tn.Name)) > 0)
                        tn.Checked = true;
                    else
                        tn.Checked = false;

                    if (tn.Nodes.Count > 0)
                        t_InsertCheckList(tn.Nodes);
            }
        }

        private void treeView1_AfterCheck(object sender, TreeViewEventArgs e)
        {
            if (AEB != null)
            {
                TreeNode tn = e.Node;
                if (tn.Checked == true)
                {
                    if (AEB.CheckList.Find(x => x == int.Parse(tn.Name)) == 0)
                    {
                        AEB.CheckList.Add(int.Parse(tn.Name));
                    }
                }
                else
                {
                    if (AEB.CheckList.Find(x => x == int.Parse(tn.Name)) > 0)
                    {
                        AEB.CheckList.Remove(int.Parse(tn.Name));
                    }
                }
            }
        }

        private void NextPageButton_Click(object sender, EventArgs e)
        {
            if ((MinInRow + AmountBookInPage) < MaxBookCountInGenre)
            {
                MinInRow += AmountBookInPage;
                PageNumber.Text = (int.Parse(PageNumber.Text) + 1).ToString();
                AddBooksToPage();
            }
        }

        private void PrePageButton_Click(object sender, EventArgs e)
        {
            if (int.Parse(PageNumber.Text) > 0)
            {
                MinInRow -= AmountBookInPage;
                PageNumber.Text = (int.Parse(PageNumber.Text) - 1).ToString();
                AddBooksToPage();
            }
        }

        private void ShowRent_CheckedChanged(object sender, EventArgs e)
        {
            PageLoad();
            if (((CheckBox)sender).Checked == true)
            {
                UserMenuStrip.Items["Open"].Visible = true;
                UserMenuStrip.Items["Viewb"].Visible = false;
            }
            else
            {
                UserMenuStrip.Items["Open"].Visible = false;
                UserMenuStrip.Items["Viewb"].Visible = true;
            }
        }

        private void Open_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count != 0)
            {
                Readf = new Reader(int.Parse(listView1.SelectedItems[0].Name), this);
                Readf.Show();
            }
        }

        private void Rent_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count != 0)
            {
                RForm = new Rent(Userid, int.Parse(listView1.SelectedItems[0].Name), this);
                RForm.Show();
            }
        }

        private void Viewb_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count != 0)
            {
                Inf = new Info(int.Parse(listView1.SelectedItems[0].Name), this);
                Inf.Show();
            }
        }


    }
}
